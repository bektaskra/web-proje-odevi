<?php
require "../conn.php";

// POST isteğinden gelen veriyi kontrol etme
if(isset($_POST['id'])) {
    // Silinecek verinin ID'sini alın
    $id = $_POST['id'];

    // Veritabanında sorgu hazırlama
    $sql = "DELETE FROM kurban WHERE id = ?";

    // Sorguyu hazırlama
    $stmt = $conn->prepare($sql);

    if($stmt) {
        // Parametreleri bağlama
        $stmt->bind_param("i", $id);

        // Sorguyu çalıştırma
        if($stmt->execute()) {
            // Başarılı yanıt döndürme
            $response = array(
                "success" => true,
                "message" => "Veri başarıyla silindi."
            );
            echo json_encode($response);
        } else {
            // Hata durumunda yanıt döndürme
            $response = array(
                "success" => false,
                "message" => "Veri silinirken bir hata oluştu: " . $conn->error
            );
            echo json_encode($response);
        }

        // Sorguyu kapatma
        $stmt->close();
    } else {
        // Hata durumunda yanıt döndürme
        $response = array(
            "success" => false,
            "message" => "Sorgu hazırlanırken bir hata oluştu: " . $conn->error
        );
        echo json_encode($response);
    }
} else {
    // Hata durumunda yanıt döndürme
    $response = array(
        "success" => false,
        "message" => "Geçersiz istek."
    );
    echo json_encode($response);
}
?>
