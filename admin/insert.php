<?php
require "../conn.php";
header('Content-Type: application/json');

// Form verilerini al
$adi = $_POST['adi'];
$satici_adi = $_POST['satici_adi'];
$yas = $_POST['yas'];
$kilo = $_POST['kilo'];
$cins = $_POST['cins'];
$fiyat = $_POST['fiyat'];

$response = ['success' => false, 'message' => ''];

// Resim dosyasını kontrol et ve yükle
if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
    $allowed = ['jpg', 'jpeg', 'png', 'gif'];
    $filename = $_FILES['image']['name'];
    $filetype = pathinfo($filename, PATHINFO_EXTENSION);
    $filesize = $_FILES['image']['size'];

    // Dosya türünü kontrol et
    if (in_array($filetype, $allowed)) {
        // Dosya boyutunu kontrol et (5MB limit)
        if ($filesize <= 5242880) {
            $new_filename = uniqid() . '.' . $filetype;
            $upload_dir = 'uploads/';

            // Hedef dizini kontrol et ve oluştur
            if (!is_dir($upload_dir)) {
                if (!mkdir($upload_dir, 0755, true)) {
                    $response['message'] = 'Yükleme dizini oluşturulamadı.';
                    echo json_encode($response);
                    exit;
                }
            }

            $upload_file = $upload_dir . $new_filename;

            if (move_uploaded_file($_FILES['image']['tmp_name'], $upload_file)) {
                // Dosya başarıyla yüklendi, veritabanına kaydet
                $image = $new_filename;
                $sql = "INSERT INTO kurban (adi, satici_adi, yas, fiyat, cins, kilo, image) VALUES ('$adi', '$satici_adi', $yas, $fiyat, '$cins', $kilo, '$image')";

                if ($conn->query($sql) === TRUE) {
                    $response['success'] = true;
                } else {
                    $response['message'] = 'Kayıt eklenirken hata oluştu: ' . $conn->error;
                }
            } else {
                $error = error_get_last();
                $response['message'] = 'Dosya yüklenemedi: ' . $error['message'];
            }
        } else {
            $response['message'] = 'Dosya boyutu 5MB sınırını aşıyor.';
        }
    } else {
        $response['message'] = 'Geçersiz dosya türü. Sadece jpg, jpeg, png ve gif dosyaları kabul edilir.';
    }
} else {
    // $_FILES['image']['error'] değerine göre hata mesajı
    $error_messages = [
        UPLOAD_ERR_INI_SIZE => 'Dosya php.ini dosyasındaki upload_max_filesize yönergesini aşıyor.',
        UPLOAD_ERR_FORM_SIZE => 'Dosya HTML formunda belirtilen MAX_FILE_SIZE yönergesini aşıyor.',
        UPLOAD_ERR_PARTIAL => 'Dosya kısmen yüklendi.',
        UPLOAD_ERR_NO_FILE => 'Hiçbir dosya yüklenmedi.',
        UPLOAD_ERR_NO_TMP_DIR => 'Geçici klasör eksik.',
        UPLOAD_ERR_CANT_WRITE => 'Diske yazma hatası.',
        UPLOAD_ERR_EXTENSION => 'Bir PHP uzantısı dosya yüklemeyi durdurdu.',
    ];

    $error_code = $_FILES['image']['error'];
    $response['message'] = 'Resim dosyası yüklenirken bir hata oluştu. Hata: ' . (isset($error_messages[$error_code]) ? $error_messages[$error_code] : 'Bilinmeyen hata kodu: ' . $error_code);
}

echo json_encode($response);
?>
