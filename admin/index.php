<?php
    session_start();
    if (!isset($_SESSION['login'])){
        echo "<script>window.location = 'login.php'</script>";
    }
    require "../conn.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Mazer Admin Dashboard</title>
    
    
    
    <link rel="shortcut icon" href="./assets/compiled/svg/favicon.svg" type="image/x-icon">
    <link rel="shortcut icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAiCAYAAADRcLDBAAAEs2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgZXhpZjpQaXhlbFhEaW1lbnNpb249IjMzIgogICBleGlmOlBpeGVsWURpbWVuc2lvbj0iMzQiCiAgIGV4aWY6Q29sb3JTcGFjZT0iMSIKICAgdGlmZjpJbWFnZVdpZHRoPSIzMyIKICAgdGlmZjpJbWFnZUxlbmd0aD0iMzQiCiAgIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjIiCiAgIHRpZmY6WFJlc29sdXRpb249Ijk2LjAiCiAgIHRpZmY6WVJlc29sdXRpb249Ijk2LjAiCiAgIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiCiAgIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIKICAgeG1wOk1vZGlmeURhdGU9IjIwMjItMDMtMzFUMTA6NTA6MjMrMDI6MDAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjItMDMtMzFUMTA6NTA6MjMrMDI6MDAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJwcm9kdWNlZCIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWZmaW5pdHkgRGVzaWduZXIgMS4xMC4xIgogICAgICBzdEV2dDp3aGVuPSIyMDIyLTAzLTMxVDEwOjUwOjIzKzAyOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9InIiPz5V57uAAAABgmlDQ1BzUkdCIElFQzYxOTY2LTIuMQAAKJF1kc8rRFEUxz9maORHo1hYKC9hISNGTWwsRn4VFmOUX5uZZ36oeTOv954kW2WrKLHxa8FfwFZZK0WkZClrYoOe87ypmWTO7dzzud97z+nec8ETzaiaWd4NWtYyIiNhZWZ2TvE946WZSjqoj6mmPjE1HKWkfdxR5sSbgFOr9Ll/rXoxYapQVik8oOqGJTwqPL5i6Q5vCzeo6dii8KlwpyEXFL519LjLLw6nXP5y2IhGBsFTJ6ykijhexGra0ITl5bRqmWU1fx/nJTWJ7PSUxBbxJkwijBBGYYwhBgnRQ7/MIQIE6ZIVJfK7f/MnyUmuKrPOKgZLpEhj0SnqslRPSEyKnpCRYdXp/9++msneoFu9JgwVT7b91ga+LfjetO3PQ9v+PgLvI1xkC/m5A+h7F32zoLXug38dzi4LWnwHzjeg8UGPGbFfySvuSSbh9QRqZ6H+Gqrm3Z7l9zm+h+iafNUV7O5Bu5z3L/wAdthn7QIme0YAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAJTSURBVFiF7Zi9axRBGIefEw2IdxFBRQsLWUTBaywSK4ubdSGVIY1Y6HZql8ZKCGIqwX/AYLmCgVQKfiDn7jZeEQMWfsSAHAiKqPiB5mIgELWYOW5vzc3O7niHhT/YZvY37/swM/vOzJbIqVq9uQ04CYwCI8AhYAlYAB4Dc7HnrOSJWcoJcBS4ARzQ2F4BZ2LPmTeNuykHwEWgkQGAet9QfiMZjUSt3hwD7psGTWgs9pwH1hC1enMYeA7sKwDxBqjGnvNdZzKZjqmCAKh+U1kmEwi3IEBbIsugnY5avTkEtIAtFhBrQCX2nLVehqyRqFoCAAwBh3WGLAhbgCRIYYinwLolwLqKUwwi9pxV4KUlxKKKUwxC6ZElRCPLYAJxGfhSEOCz6m8HEXvOB2CyIMSk6m8HoXQTmMkJcA2YNTHm3congOvATo3tE3A29pxbpnFzQSiQPcB55IFmFNgFfEQeahaAGZMpsIJIAZWAHcDX2HN+2cT6r39GxmvC9aPNwH5gO1BOPFuBVWAZue0vA9+A12EgjPadnhCuH1WAE8ivYAQ4ohKaagV4gvxi5oG7YSA2vApsCOH60WngKrA3R9IsvQUuhIGY00K4flQG7gHH/mLytB4C42EgfrQb0mV7us8AAMeBS8mGNMR4nwHamtBB7B4QRNdaS0M8GxDEog7iyoAguvJ0QYSBuAOcAt71Kfl7wA8DcTvZ2KtOlJEr+ByyQtqqhTyHTIeB+ONeqi3brh+VgIN0fohUgWGggizZFTplu12yW8iy/YLOGWMpDMTPXnl+Az9vj2HERYqPAAAAAElFTkSuQmCC" type="image/png">
    


  <link rel="stylesheet" href="./assets/compiled/css/app.css">
  <link rel="stylesheet" href="./assets/compiled/css/app-dark.css">
  <link rel="stylesheet" href="./assets/compiled/css/iconly.css">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

</head>

<body>
    <script src="assets/static/js/initTheme.js"></script>
    <div id="app">
        <div id="sidebar">
            <div class="sidebar-wrapper active">
    <div class="sidebar-header position-relative">
        <div class="d-flex justify-content-between align-items-center">
            <div class="logo">
                <a href="index.html">
                    <h5>Kurbanbayii <br> Yönetim</h5>
                </a>
            </div>
            <div class="theme-toggle d-flex gap-2  align-items-center mt-2">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true"
                    role="img" class="iconify iconify--system-uicons" width="20" height="20"
                    preserveAspectRatio="xMidYMid meet" viewBox="0 0 21 21">
                    <g fill="none" fill-rule="evenodd" stroke="currentColor" stroke-linecap="round"
                        stroke-linejoin="round">
                        <path
                            d="M10.5 14.5c2.219 0 4-1.763 4-3.982a4.003 4.003 0 0 0-4-4.018c-2.219 0-4 1.781-4 4c0 2.219 1.781 4 4 4zM4.136 4.136L5.55 5.55m9.9 9.9l1.414 1.414M1.5 10.5h2m14 0h2M4.135 16.863L5.55 15.45m9.899-9.9l1.414-1.415M10.5 19.5v-2m0-14v-2"
                            opacity=".3"></path>
                        <g transform="translate(-210 -1)">
                            <path d="M220.5 2.5v2m6.5.5l-1.5 1.5"></path>
                            <circle cx="220.5" cy="11.5" r="4"></circle>
                            <path d="m214 5l1.5 1.5m5 14v-2m6.5-.5l-1.5-1.5M214 18l1.5-1.5m-4-5h2m14 0h2"></path>
                        </g>
                    </g>
                </svg>
                <div class="form-check form-switch fs-6">
                    <input class="form-check-input  me-0" type="checkbox" id="toggle-dark" style="cursor: pointer">
                    <label class="form-check-label"></label>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true"
                    role="img" class="iconify iconify--mdi" width="20" height="20" preserveAspectRatio="xMidYMid meet"
                    viewBox="0 0 24 24">
                    <path fill="currentColor"
                        d="m17.75 4.09l-2.53 1.94l.91 3.06l-2.63-1.81l-2.63 1.81l.91-3.06l-2.53-1.94L12.44 4l1.06-3l1.06 3l3.19.09m3.5 6.91l-1.64 1.25l.59 1.98l-1.7-1.17l-1.7 1.17l.59-1.98L15.75 11l2.06-.05L18.5 9l.69 1.95l2.06.05m-2.28 4.95c.83-.08 1.72 1.1 1.19 1.85c-.32.45-.66.87-1.08 1.27C15.17 23 8.84 23 4.94 19.07c-3.91-3.9-3.91-10.24 0-14.14c.4-.4.82-.76 1.27-1.08c.75-.53 1.93.36 1.85 1.19c-.27 2.86.69 5.83 2.89 8.02a9.96 9.96 0 0 0 8.02 2.89m-1.64 2.02a12.08 12.08 0 0 1-7.8-3.47c-2.17-2.19-3.33-5-3.49-7.82c-2.81 3.14-2.7 7.96.31 10.98c3.02 3.01 7.84 3.12 10.98.31Z">
                    </path>
                </svg>
            </div>
            <div class="sidebar-toggler  x">
                <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
            </div>
        </div>
    </div>
    <div class="sidebar-menu">
        <ul class="menu">

            <li
                class="sidebar-item">
                <a href="../index.php" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Kurbanbayii'e dön</span>
                </a>
                

            </li>
            <li
                class="sidebar-item active ">
                <a href="index.html" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Ana Sayfa</span>
                </a>


            </li>
            
            <li
                class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i class="bi bi-stack"></i>
                    <span>Kurbanlıklar</span>
                </a>
                
                <ul class="submenu ">
                    
                    <li class="submenu-item  ">
                        <a href="component-accordion.html" class="submenu-link">Liste</a>
                        
                    </li>
                    
                    <li class="submenu-item  ">
                        <a href="component-alert.html" class="submenu-link disabled">Ekle</a>
                    </li>
                </ul>
        </ul>
    </div>
</div>
        </div>
        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>
            
<div class="page-heading">
    <h3>Kurbanbayii Yönetim Paneli</h3>
</div> 
<div class="page-content"> 
    <section class="row">
        <div class="col-12 col-lg-9">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <div class="row">
                                <div class="col-6">
                                    <h4>Tüm Kurbanlıklar</h4>
                                </div>
                                <div class="col-6 d-flex justify-content-end">
                                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalNew">Yeni Kurbanlık</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">Görsel</th>
                                            <th scope="col">Hayvan Adı</th>
                                            <th scope="col">Satıcı Adı</th>
                                            <th scope="col">Yaş</th>
                                            <th scope="col">Cins</th>
                                            <th scope="col">Kilo</th>
                                            <th scope="col">Fiyat</th>
                                            <th scope="col">Eylemler</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $sql = "SELECT * FROM kurban";
                                        $result = mysqli_query($conn, $sql);

                                        if (mysqli_num_rows($result) > 0) {
                                            // Verileri yazdırma
                                            while($row = mysqli_fetch_assoc($result)) {
                                                ?>
                                                <tr data-id="<?= $row['id'] ?>" data-adi="<?= $row['adi'] ?>" data-satici_adi="<?= $row['satici_adi'] ?>" data-yas="<?= $row['yas'] ?>" data-cins="<?= $row['cins'] ?>" data-kilo="<?= $row['kilo'] ?>" data-fiyat="<?= $row['fiyat'] ?>" data-image="<?= $row['image'] ?>">
                                                    <td><img style="width: 64px;height: 64px;border-radius: 5px" src="uploads/<?= $row['image'] ?>"></td>
                                                    <td><?= $row['adi'] ?></td>
                                                    <td><?= $row['satici_adi'] ?></td>
                                                    <td><?= $row['yas'] ?></td>
                                                    <td><?= $row['cins'] ?></td>
                                                    <td><?= $row['kilo'] ?></td>
                                                    <td><?= $row['fiyat'] ?></td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic outlined example">
                                                            <button type="button" class="btn btn-outline-warning btn-edit" data-bs-toggle="modal" data-bs-target="#modalEdit">Düzenle</button>
                                                            <button type="button" class="btn btn-outline-danger btn-delete">Kaldır</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            echo "Kayıt bulunamadı.";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <script>
                                        $(document).ready(function() {
                                            // Kaldırma butonuna tıklama olayını yakalama
                                            $('.btn-delete').on('click', function() {
                                                // Kullanıcıya onaylama iletişim kutusu gösterme
                                                if (confirm('Bu veriyi kaldırmak istediğinizden emin misiniz?')) {
                                                    // Evet'e tıklanırsa veriyi kaldırma işlemini gerçekleştirme
                                                    var row = $(this).closest('tr');
                                                    var id = row.data('id');

                                                    // Kaldırma işlemini AJAX ile sunucuya gönderme
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: 'http://localhost/kurbanlik/admin/delete.php',
                                                        data: { id: id }, // Kaldırılacak verinin ID'sini sunucuya gönderme
                                                        dataType: 'json',
                                                        success: function(response) {
                                                            if (response.success) {
                                                                alert('Veri başarıyla kaldırıldı!');
                                                                row.remove(); // Tablodan veriyi kaldırma
                                                            } else {
                                                                alert('Veri kaldırılırken bir hata oluştu: ' + response.message);
                                                            }
                                                        },
                                                        error: function(xhr, status, error) {
                                                            alert('Bir hata oluştu. Lütfen tekrar deneyin.');
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12 col-lg-3">
            <div class="card">
                <div class="card-body py-4 px-4">
                    <div class="d-flex align-items-center">
                        <div style="padding: 10px;background-color: #e8e8e8;border-radius: 50%;width: 90px;height: 90px">
                            <i style="font-size: 50px;margin-left:8px" class="bi bi-person-fill-gear"></i>
                        </div>
                        <div class="ms-3 name">
                            <h5 class="font-bold"><?= $_SESSION['name']." ".$_SESSION['surname'] ?></h5>
                            <h6 class="text-muted mb-0">@<?= $_SESSION['username'] ?></h6>
                            <button onclick="window.location = 'exit.php'" class="btn btn-danger">Oturumu Kapat</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

            <!-- Modal -->
            <div class="modal fade" id="modalNew" tabindex="-1" aria-labelledby="modalNewLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modalNewLabel">Kurbanlık Ekleme Sistemi</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form enctype="multipart/form-data" id="kurbanlikEklemeForm">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input name="adi" type="text" class="form-control" id="insert_adi">
                                            <label for="adi">Kurbanlık Adı</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input name="satici_adi" type="text" class="form-control" id="insert_saticiAdi">
                                            <label for="saticiAdi">Satıcı Adı</label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-floating">
                                            <input name="image" type="file" class="form-control" id="insert_image">
                                            <label for="image">Resim</label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="yas" value="2" min="2" type="number" class="form-control" id="insert_yas">
                                            <label for="yas">Yaş</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="kilo" min="100" value="100" type="number" class="form-control" id="insert_kilo">
                                            <label for="kilo">Kilo</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <select name="cins" class="form-control" id="insert_cins">
                                                <option>Sarı İnek</option>
                                                <option>Kara İnek</option>
                                                <option>Kahverengi İnek</option>
                                                <option>Milka İnek</option>
                                            </select>
                                            <label for="cins">Cins</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="fiyat" type="number" class="form-control" id="insert_fiyat">
                                            <label for="fiyat">Fiyat</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Ekle</button>
                            </form>
                            <script>
                                $(document).ready(function() {
                                    $('#kurbanlikEklemeForm').on('submit', function(e) {
                                        e.preventDefault(); // Formun normal gönderilmesini durdur

                                        // Form verilerini topla
                                        var formData = new FormData(this);

                                        // Ajax isteği gönder
                                        $.ajax({
                                            type: 'POST',
                                            url: 'insert.php',
                                            data: formData,
                                            contentType: false,
                                            processData: false,
                                            dataType: 'json',
                                            success: function(response) {
                                                if(response.success) {
                                                    alert('Kayıt başarıyla eklendi!');
                                                    $('#kurbanlikEklemeForm')[0].reset();
                                                    window.location.reload();
                                                } else {
                                                    alert('Kayıt eklenirken bir hata oluştu: ' + response.message);
                                                }
                                            },
                                            error: function() {
                                                alert('Bir hata oluştu. Lütfen tekrar deneyin.');
                                            }
                                        });
                                    });
                                });
                            </script>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Modal Edit -->
            <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEditLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modalEditLabel">Kurbanlık Düzenleme Sistemi</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form enctype="multipart/form-data" id="kurbanlikEditForm">
                                <div style="">
                                    <input id="edit_id" name="id">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input name="adi" type="text" class="form-control" id="edit_adi">
                                            <label for="adi">Kurbanlık Adı</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input name="satici_adi" type="text" class="form-control" id="edit_saticiAdi">
                                            <label for="saticiAdi">Satıcı Adı</label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <!--
                                <div class="row">
                                    <div class="col-12">
                                        <h6>Görsel</h6>
                                        <div class="row">
                                            <div class="col-4">
                                                <button name="remove_image" class="btn btn-danger">Görseli Kaldır</button>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-floating">
                                                    <input name="reimage" type="file" class="form-control" id="image">
                                                    <label for="image">Yeni Resim</label>
                                                </div>
                                                <p class="text-muted">Bu boşluğu doldurmazsanız orjinal resim değişmez.</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                -->
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="yas" value="2" min="2" type="number" class="form-control" id="edit_yas">
                                            <label for="yas">Yaş</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="kilo" min="100" value="100" type="number" class="form-control" id="edit_kilo">
                                            <label for="kilo">Kilo</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <select name="cins" class="form-control" id="edit_cins">
                                                <option>Sarı İnek</option>
                                                <option>Kara İnek</option>
                                                <option>Kahverengi İnek</option>
                                                <option>Milka İnek</option>
                                            </select>
                                            <label for="cins">Cins</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-floating">
                                            <input name="fiyat" type="number" class="form-control" id="edit_fiyat">
                                            <label for="fiyat">Fiyat</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Güncelle</button>
                            </form>
                            <script>
                                $(document).ready(function() {
                                    // Düzenle butonuna tıklama olayını yakalama
                                    $('.btn-edit').on('click', function() {
                                        var row = $(this).closest('tr');
                                        var id = row.data('id');
                                        var adi = row.data('adi');
                                        var saticiAdi = row.data('satici_adi');
                                        var yas = row.data('yas');
                                        var cins = row.data('cins');
                                        var kilo = row.data('kilo');
                                        var fiyat = row.data('fiyat');

                                        // Modal form alanlarına verileri yükle
                                        $('#edit_id').val(id);
                                        $('#edit_adi').val(adi);
                                        $('#edit_saticiAdi').val(saticiAdi);
                                        $('#edit_yas').val(yas);
                                        $('#edit_cins').val(cins);
                                        $('#edit_kilo').val(kilo);
                                        $('#edit_fiyat').val(fiyat);
                                    });

                                    // Form gönderme olayını yakalama
                                    $('#kurbanlikEditForm').on('submit', function(e) {
                                        e.preventDefault(); // Formun normal gönderilmesini durdur

                                        // Form verilerini topla
                                        var formData = new FormData(this);

                                        console.log("Form Data: ", formData); // Hata ayıklama için form verilerini konsola yazdır

                                        for (var [key, value] of formData.entries()) {
                                            console.log("Form Data:", key, ":", value);
                                        }

                                        // Ajax isteği gönder
                                        $.ajax({
                                            type: 'POST',
                                            url: 'update.php',
                                            data: formData,
                                            contentType: false,
                                            processData: false,
                                            dataType: 'json',
                                            success: function(response) {
                                                console.log("AJAX Success Response: ", response); // Hata ayıklama için başarı yanıtını konsola yazdır
                                                if(response.success) {
                                                    alert('Kayıt başarıyla güncellendi!');
                                                    location.reload(); // Sayfayı yeniden yükle
                                                } else {
                                                    alert('Kayıt güncellenirken bir hata oluştu: ' + response.message);
                                                }
                                            },
                                            error: function(xhr, status, error) {
                                                console.log("AJAX Error: ", xhr, status, error); // Hata ayıklama için hata bilgilerini konsola yazdır
                                                alert('Bir hata oluştu. Lütfen tekrar deneyin.');
                                            }
                                        });
                                    });
                                });
                            </script>

                        </div>
    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/static/js/components/dark.js"></script>
    <script src="assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    
    
    <script src="assets/compiled/js/app.js"></script>
    

    
<!-- Need: Apexcharts -->
<script src="assets/extensions/apexcharts/apexcharts.min.js"></script>
<script src="assets/static/js/pages/dashboard.js"></script>

</body>

</html>