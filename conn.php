<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "kurbanlik";

// Bağlantı oluşturma
$conn = mysqli_connect($servername, $username, $password, $database);

// Bağlantıyı kontrol etme
if (!$conn) {
    die("Bağlantı hatası: " . mysqli_connect_error());
}