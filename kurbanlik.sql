-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 30 May 2024, 20:58:50
-- Sunucu sürümü: 10.4.28-MariaDB
-- PHP Sürümü: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `kurbanlik`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanici`
--

CREATE TABLE `kullanici` (
  `id` int(11) NOT NULL,
  `kullanici_adi` varchar(35) NOT NULL,
  `parola` text NOT NULL,
  `ad` varchar(50) NOT NULL,
  `soyad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `kullanici`
--

INSERT INTO `kullanici` (`id`, `kullanici_adi`, `parola`, `ad`, `soyad`) VALUES
(1, 'a', '0cc175b9c0f1b6a831c399e269772661', 'Bektaş', 'Kara');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kurban`
--

CREATE TABLE `kurban` (
  `id` int(11) NOT NULL,
  `adi` varchar(50) NOT NULL,
  `satici_adi` varchar(50) NOT NULL,
  `yas` tinyint(4) NOT NULL,
  `fiyat` double NOT NULL,
  `cins` varchar(25) NOT NULL,
  `kilo` smallint(6) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `kurban`
--

INSERT INTO `kurban` (`id`, `adi`, `satici_adi`, `yas`, `fiyat`, `cins`, `kilo`, `image`) VALUES
(5, 'Bu fursat kaçmaz gel abi gel', 'Hasan emmi', 4, 102000, 'Kara İnek', 1000, '6658c9c885352.jpg'),
(6, 'A++++ kalitede kurbanlık', 'Eşşekoğulları Çiftliği', 3, 149952, 'Kahverengi İnek', 100, '6658ca420421d.jpg'),
(7, 'en iyi kurban', 'cüneyt c', 2, 1020, 'Kara İnek', 980, '6658cae9add49.png');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `kullanici`
--
ALTER TABLE `kullanici`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kurban`
--
ALTER TABLE `kurban`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `kullanici`
--
ALTER TABLE `kullanici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `kurban`
--
ALTER TABLE `kurban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
