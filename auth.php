<?php
require "conn.php";
// Kullanıcı adını $_POST ile al
if(isset($_POST['username'])) {
    $username = $_POST['username'];

    // Kullanıcı adını veritabanında kontrol et
    $sql = "SELECT * FROM kullanici WHERE kullanici_adi = '$username'";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) > 0) {
        // Kullanıcı adı veritabanında bulundu, şimdi parolayı kontrol et
        if(isset($_POST['parola'])) {
            $password = md5($_POST['parola']); // Kullanıcının girdiği parolayı md5 ile hash'le

            // Veritabanından parolayı al
            $row = mysqli_fetch_assoc($result);
            $db_password = $row['parola']; // Veritabanındaki hash'lenmiş parola

            // Parolayı kontrol et
            if($password == $db_password) {
                echo "Parola doğru. Giriş başarılı!";
            } else {
                echo "Parola yanlış!";
            }
        } else {
            echo "Parola alanı boş.";
        }
    } else {
        echo "Kullanıcı adı bulunamadı.";
    }
} else {
    echo "Kullanıcı adı alanı boş.";
}